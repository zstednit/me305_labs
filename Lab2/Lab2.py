'''
@page "Lab 2: Incremental Encoders"

@author Zachary Stednitz
@author Jason Davis
@author Conor Fraser
@author Solie Grantham

@date 12/8/2021

@section sec_overview Overview

This lab used quadrature encoders to read position of two different motors.
While the code seen in the documentation is the most up to date versions of files
we have used after this second lab had concluded, you can find the unmodified files
at this respository: \n
    https://bitbucket.org/zstednit/me305_labs/src/master/Lab2/ \n
    
    @image html "Lab2TaskDiagram.png" \n
    @image html "Lab2FSM.png" 
    
This lab uses a multi tasking approach with distinct tasks defined separately from
the main file and then imported.
    
To view documentation for the different files, visit: \n
    encoder.py \ref \n
    main.py \ref \n
    shares.py \ref \n
    task_encoder.py \ref \n
    task_user.py \ref \n
    
''' 