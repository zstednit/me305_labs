# -*- coding: utf-8 -*-

''' @file                       encoder.py
    @brief                      A driver for reading from Quadrature Encoders
    @details
    @author                     Zachary Stednitz
    @author                     Solie Grantham
    @author                     Jason Davis
    @date                       October 7, 2021
'''
import pyb
import time

class Encoder():
    ''' @brief                  Interface with quadrature encoders
        @details                Fixes overflow for encoder counting. 
                                Stops the reset of the counter so that 
                                the encoder can spin infinitely. Delta is 
                                calculated as change in timer count. Checks 
                                magnitude of delta and interprets change to 
                                adjust to postion.
    '''
    
    def __init__(self, pinA, pinB, timNum, ID = None):
        ''' @brief              Interface with quadrature encoders
            @details            Assigns timer and channel to each pin.
                                Initializes parameters that you can change when 
                                you create an encoder object. 
            @param pinA         Name of pin referencing encoder 1
            @param pinB         Name of pin referencing encoder 2
            @param timNum       Timer number
            @param ID           Optional condition to assign hardware
                                identification.
        '''
        self.pinA = pinA
        self.pinB = pinB
        self.timNum = timNum
        self.position = 0     
        self.delta = 0
        self.period = 65535 + 1
        
        # Optional parameter to assign an ID to the hardware
        self.ID = ID if ID is not None else None
        
        # each pair of pins gets a timer
        self.encoderTimer = pyb.Timer(timNum, prescaler = 0, period = 65535)
        
        # each pin gets a channel
        self.encoderTimer.channel(1, pyb.Timer.ENC_AB, pin = pinA)
        self.encoderTimer.channel(2, pyb.Timer.ENC_AB, pin = pinB) 
        
        self.prev_count = self.encoderTimer.counter()
        
    def update(self):
        ''' @brief              Updates encoder position and delta
            @details            If the value of delta is greater than or equal
                                to the period/2, the encoder value will shift 
                                downards by the value of the period. If the 
                                value of delta is less than or equal to
                                -period/2, the encoder value will shoft upwards 
                                by the value of the period.
        '''
        current_count = self.encoderTimer.counter()
        self.delta = current_count - self.prev_count
        
        # This logic handles counter overflow
        if (self.delta >= self.period/2):
            self.delta -= self.period()
        if self.delta <= (-1 * self.period/2):
            self.delta += self.period
            
        self.prev_count = current_count
        self.position += self.delta
        
        
    def get_position(self):
        ''' @brief              Returns encoder position
            @details            Returns value correlated with self.position class
            @return             The position of the encoder shaft
        '''
        return self.position
    
    
    def set_position(self, position):
        ''' @brief              Updates encoder position and delta
            @details            References self.position class from constructor without changing value
            @param  position    The new position of the encoder shaft 
        '''
        self.position = position
        
    def get_delta(self):
        ''' @brief              Returns encoder delta
            @details            Returns value correlated with self.delta class
            @return             The change in position of the encoder shaft
                                between the most two recent updates
        '''
        return self.delta
    
    def set_encoder_ID(self, ID):
        ''' @brief              Sets encoder ID to variable ID
            @details            References self ID class without changing it
            @param ID           The new ID value assignment for the encoder
        '''
        self.ID = ID
        
    def get_encoder_ID(self):
        ''' @brief              Returns the set encoder ID
            @details            Returns value correlated with self.ID class
            @return             New ID of the encoder as it has been assigned
        '''
        return self.ID
        