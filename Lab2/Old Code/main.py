# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 20:33:14 2021

@author: Jason Davis
"""
import pyb, time
import task_ui, encoder, task_encoder

# instantiating our encoder
encoderA = encoder.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, ID="ENCODER_A")
# encoderB = encoder.Encoder(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 3, ID="ENCODER_B")

# instantiating the user interface
# the ui will return a character representing the desired task and pass it to task_encoder
interface = task_ui.Task_ui()

# instantiating a task object for each encoder
taskA = task_encoder.Task_encoder(encoderA)
# taskB = task_encoder.Task_encoder(encoderB)

while (True):
    try:
        
        # requesting an action from the user
        userRequest = interface.displayMenu()
        
        # passing that request to the task handler for encoder A
        taskA.scheduleUserRequest(userRequest)
        
        # passing that request to the task handler for encoder B
        # taskB.handleUserRequest(userRequest)
        
    except KeyboardInterrupt:
        break
print('Program ending, have a nice day!')