# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 16:23:34 2021

@author:                        Jason Davis
"""

class Task_ui:
    ''' @brief                  Interacts with the user to obtain requests
        @details                Obtains requests from the user in the form of a list
                                of characters and/or numbers.
    '''
    
    def __init__(self):
       pass
    
    def displayMenu(self):
        
        print('Please select an option from the menu:')
        print ('[z]   Zero the encoder position')
        print ('[p]   Display encoder position')
        print ('[d]   Display delta for encoder')
        print ('[g]   Collect encoder data for 30 seconds and display output')
        print ('[s]   End data collection prematurely')
        
        option = input()
        
        validInput = False
        
        # validates user input here before returning the request
        if (option == 'z' or option == 'p' or option == 'd' or option == 'g' or option == 's'):
            validInput = True
            
        while (not validInput):
            print('Invalid Input! Please select an option from the menu:')
            print ('[z]   Zero the encoder position')
            print ('[p]   Display encoder position')
            print ('[d]   Display delta for encoder')
            print ('[g]   Collect encoder data for 30 seconds and display output')
            print ('[s]   End data collection prematurely')
            
            option = input()
            
            if (option == 'z' or option == 'p' or option == 'd' or option == 'g' or option == 's'):
                validInput = True
                
        return option
    
if __name__ == "__main__":
    print('Welcome to the lab2 encoder menu!')   