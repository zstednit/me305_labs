# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 13:13:05 2021

@author: Jason Davis
"""

class Task_encoder:
    '''@brief                               Receives input from the user interface via "main" and executes the request
       @details                             Accepts input from the user and handles the request.  Since input arrives pre-validated,
                                            no character/data validation is performed here
    '''
    
    def __init__(self, encoder):
        self.encoder = encoder
        self.taskQueue = []
        
    #using a queue to schedule requests
    def scheduleUserRequest(self, action):
        self.taskQueue.append(action)
        self.handleUserRequest()
        
    # this is called from main
    def handleUserRequest(self):
        
        # if (not self.taskQueue.empty()):
            
        action = self.taskQueue.pop(0)
        
        print("***ACTION PERFORMED: ")        

        if (action == 'z'):
            self.zeroEncoderPosition()
        elif (action == 'p'):
            self.displayEncoderPosition()
        elif (action == 'd'):
            self.displayEncoderDelta()
        elif (action == 'g'):
            self.displayEncoderSnapshot()
        elif (action == 's'):
            self.haltAndReturnSnapshot()
        
    # These methods are accessed only from within task_encoder
    def zeroEncoderPosition(self):
        self.encoder.set_position(0)
        print("{0} position set to zero\n".format(self.encoder.get_encoder_ID()))
        
    def displayEncoderPosition(self):
        print("{0} position is: {1}\n".format(self.encoder.get_encoder_ID(), self.encoder.get_position()))
        
    def displayEncoderDelta(self):
        print("{0} changed position by: {1}\n".format(self.encoder.get_encoder_ID(), self.encoder.get_delta()))
    
    # consider outputting the snapshots to a txt file and add an option to the UI
    # to view it on demand 
    def displayEncoderSnapshot(self):
        print('Collects encoder data for 30 seconds and displays the output')
        print('Method under construction\n')
         
    def haltAndReturnSnapshot(self):
        print('Halts the gathering of data from the encoders and displays the result')
        print('Method under construction\n')
    
     