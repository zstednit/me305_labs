# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 20:33:14 2021

@author:                    Jason Davis
@author                     Zachary Stednitz
@author                     Solie Grantham

@file                       main.py
@brief                      Create encoder objects to get encoder positions
@details                    Runs an encoder task and user task that allows for 
                            reading and storing of encoding positions that can 
                            then be output according to user input. The data is
                            exchanged between the tasks and the main program by
                            shares and tuples. Pictured below is the task diagram
                            and finite state machine used for structuring this 
                            code. 
                            \image html Lab2TaskDiagram.png
                            \image html Lab2FSM.png
"""
import pyb
import shares
import task_user, encoder, task_encoder

# instantiating our encoders
encoderA = encoder.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, ID="ENCODER_A")
# encoderB = encoder.Encoder(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 3, ID="ENCODER_B")

# instantiating a share object for the task_encoder
encoder_share = shares.Share(0)

# a share object for the output
# list of tuples
output_share = shares.Share(0)

# instantiating the user interface
# the ui will return a character representing the desired task and pass it to task_encoder
task_1 = task_user.Task_User('USER', 10000, encoder_share, output_share, dbg=False)

# instantiating a task object for each task
task_2A = task_encoder.Task_Encoder('ENC_A', 10000, encoderA, encoder_share, output_share)
# task_2B = task_encoder.Task_Encoder('ENC_B', 10000, encoderB, encoder_share, output_share)

# create a task list
# taskList = [task_1, task_2A, task_2B]
taskList = [task_1, task_2A]

while (True):
    try:
        encoderA.update()
        # encoderB.update()
        
        for task in taskList:
            task.run()
        
    except KeyboardInterrupt:
        break
print('Program ending, have a nice day!')