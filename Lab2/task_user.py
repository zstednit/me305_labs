# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 14:07:04 2021

@author:                        Jason Davis
@author                         Zachary Stednitz
@author                         Solie Grantham
"""

'''@file                       task_user.py
   @brief                      User interface task for cooperative multitasking example.
   @details                    Implements a finite state machine
'''

import utime, pyb, gc
from micropython import const

## List of possible encoder states
S0_init = const(0)
S1_waitForInput = const(1)

class Task_User():
    '''@brief                      User interface task for cooperative multitasking example.
       @details                    Implements a finite state machine
    '''
    
    def __init__(self, taskID, period, encoder_share, output_share, dbg=False):
        '''@brief                  Constructs an encoder task.
           @details                The encoder task is implemented as a finite state
                                   machine.
           @param taskID           The name of the task
           @param period           The period, in microseconds, between runs of 
                                   the task.
           @param encoder_share    A shares.Share object representing encoder, holds users instructions
           @param output_share     A shares.Share object representing the encoder output
           @param dbg              A boolean flag used to enable or disable debug
                                   messages printed over the VCP
        '''
        ## The name of the task
        self.taskID = taskID
        ## The period (in us) of the task
        self.period = period
        ## A shares.Share object representing encoder
        self.encoder_share = encoder_share
        
        ## A shares.Share object representing the encoder output
        self.output_share = output_share
        
        ## A flag indicating if debugging print messages display
        self.dbg = dbg
        
        ## A serial port to use for user I/O
        ## A virtual "bucket" in which to dump user input
        ## Reading from 'ser' is like checking the bucket for any new input, only
        ## we don't halt the program in event that none is entered
        self.ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_init
        ## The number of runs of the state machine
        # self.runs = 0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        # the list where the positions are stored
        self.output = []
        # the list where the times are stored
        self.times = []
        
        gc.enable()
        
    def run(self):
        '''@brief                  Runs one iteration of the FSM
           @details                Displays input menu and reads and decodes 
                                   character inputs from user.
                                    
                                   If the user inputs 'z' or 'Z' the enocder's
                                   postion will be zeroed.
                                   If the user inputs 'p' or 'P' the encoder will
                                   locate the encoder positional value.
                                   If the user inputs 'd' or 'D' the delta 
                                   position will be returned.
                                   If the user inputs 'g' or 'G', data will be 
                                   acquired from the encoder.
                                   If the user inputs 's' or 'S' data colection
                                   will be stopped prematurely.
                                    
            @return                Values will be returned based on input(see details).
        '''
        gc.collect()
        current_time = utime.ticks_us()
        # print("encoder share value: {0}".format(self.encoder_share.read()))
        # print("state: {0}".format(self.state))
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0_init:
                
                # displaying the menu and then advancing to state 1 of the
                # task_user FSM
                self.displayMenu()
                self.transition_to(S1_waitForInput)       
                
        # we have to change the value in the VCM, otherwise the program will run in an inifinite loop
        # this is why we are reading letters and writing/sharing numbers
            elif self.state == S1_waitForInput:
                # print ('vcp: {0}'.format(self.ser.read(1).decode()))
                # print ('encoder share: {0}'.format(self.encoder_share.read()))
                if (self.ser.any()):
                    # print ("something waiting in VCP")
                    char_in = self.ser.read(1).decode()
                    # char_in = self.ser.read(1)
                    # print(char_in)
                    # print ('{0} waiting in the VCP'.format(char_in))
                    if(char_in == 'z' or char_in == 'Z'):
                        # passing the character to the task_encoder
                        self.encoder_share.write(1)
                        
                    elif(char_in == 'p' or char_in == 'P'):
                        self.encoder_share.write(2)
                        
                    elif (char_in == 'd' or char_in == 'D'):
                        self.encoder_share.write(3)
                        
                    elif (char_in == 'g' or char_in == 'G'):
                        print ('Beginning data collection...')
                        
                        self.start_time = current_time
                        
                        # print ("start time: {0}".format(self.start_time))
                        self.encoder_share.write(4)
                        
                    # elif (self.encoder_share.read() == 'c'):
                        
                    #     # each line in the list is a tuple consisting of the current time and the encoder position
                    #     self.output.append((str(round(float(current_time/1000000))), str(self.output_share.read())))
                        
                    #     if (utime.ticks_diff(current_time, self.start_time) >= 30):
                    #         self.encoder_share.write('s') 
                    #     else:
                    #         self.encoder_share.write(4)                                              
                        
                    elif (char_in == 's' or char_in == 'S'):
                        print('*************** Data Output *************')
                        print('-----------------------------------------')
                        print('Time [s] ,      Encoder 1 Position [ticks]')
                        
                        #cycling through the output list to display it on the screen
                        
                        for i in range(0, len(self.output)):
                            print("{0}   ,          {1}".format(round(self.times[i], 2), self.output[i]))
                                            
                        print('***End of data collection')
                        self.encoder_share.write(None) # clear the encoder share
                        self.output_share.write(None) # clear the output share
                        self.times = [] # clear the times
                        self.output = [] #clear the array
                        # gc.collect()
                    
                    # input validation done here
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
                
                if (self.encoder_share.read() == 'c'):
                    # print('adding an entry')
                    # each line in the list is a tuple consisting of the current time and the encoder position
                    gc.collect()
                    self.times.append(current_time/1000000)
                    gc.collect()
                    self.output.append(self.output_share.read())
                    self.output_share.write(None)
                    # print("time diff: {0}".format((utime.ticks_diff(current_time, self.start_time)/1000000)))
                    if ((utime.ticks_diff(current_time, self.start_time)/1000000) >= 30):
                        self.encoder_share.write('s') 
                    else:
                        self.encoder_share.write(4) 
                        
                elif (self.encoder_share.read() == 's' or self.encoder_share.read() == 'S'):
                    print('*************** Data Output *************')
                    print('-----------------------------------------')
                    print('Time [s] ,      Encoder 1 Position [ticks]')
                    
                    #cycling through the output list to display it on the screen
                    
                    for i in range(0, len(self.output)):
                        print("{0}   ,          {1}".format(round(self.times[i], 2), self.output[i]))
                    
                    print('***End of data collection')
                    self.encoder_share.write(None) # clear the encoder share
                    self.output_share.write(None) # clear the output share
                    self.times = [] # clear the times
                    self.output = [] #clear the array
                    # gc.collect()
                    
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            # self.runs += 1
    
    def displayMenu(self):
        '''
         @brief                  Displays menu of potential user input
         @details                Shows user acceptable entries for the system
         @return                 Returns a menu of instructions for user input
        '''
        print('Please select an option from the menu:')
        print ('[z]   Zero the encoder position')
        print ('[p]   Display encoder position')
        print ('[d]   Display delta for encoder')
        print ('[g]   Collect encoder data for 30 seconds and display output')
        print ('[s]   End data collection prematurely')
    
    def transition_to(self, new_state):
        '''@brief              Transitions the FSM to a new state
           @details            Optionally a debugging message can be printed
                               if the dbg flag is set when the task object is created.
           @param              new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.taskID ,self.state,new_state))
        self.state = new_state