# -*- coding: utf-8 -*-
''' @file                       closed_loop_controller.py
    @brief                      A closed loop controller driver for the balancing ball ME 305 term project.
    @details                    Includes a closed loop controller class. The class contains an init function and eleven supporting functions.
    @details                    The class allows the gains to be modified in real time through the user interface.
    @details                    The torqe and duty cycles for each motor have associated functions to calculate and return their values to be passed to the motor driver.
    @author                     Jason Davis
    @author                     Conor Fraser
    @author                     Solie Grantham
    @author                     Zachary Stednitz
    @date                       December 8th, 2021
'''

import os, gc
import gainVector

# approach:
    # take in position of ball from panel as x and y
    # take in velocity of ball from panel as x_dot and y_dot
    # take in theta of panel in x and y theta values
    # take in theta_dot of panel from x and y direction theta_dot values
    
    # compute Tx from x, x_dot, theta_x, theta_dot_x with the gain equation
    # compute Ty from y, y_dot, theta_y, theta_dot_y with the gain equation
    
    # compute duty cycle x with Tx
    # compute duty cycel y with Ty
    
    # write dutyX to motor share
    # write dutyY to motor share

# R and Kt are found on the motor data sheet. They can be manually inputed without being pulled from another final

class closedLoopController:
    ''' @brief                Class that contains the functions needed to modify the gains and compute the torque and duty cycles for the motors.
        @details                 There are active and toggleActive functions that are used in out task user to enable closed loop motor control.
        @details                 The kx and ky gain matrices are created in the intializedGainMatrices function.
        @details                 The getGainMatrices, displayGainMatrix, modifyGainMatrix, and saveGainMatrix functions work with the task user so fine tuning of the gain values can be achieved in real time without a program restart.
        @details                 The computeTorques, getTx, and getTy functions commpute the torques and then spit out torque values which can be used for debugging.
        @details                 The getDutyVector returns the correct duty cycles for the motors so that the panel can be balanced.
    '''
    
    def __init__(self, option):
        ''' @brief             Initialize self variables for the closedLoopController class.
            @details           Sets the self variables for the constants in the duty cycle calculation.
            @details           Prints the updated Kx and Ky gain matrices. They will be updated in real time and printed to the Putty window to assist with gain value calibration.
            @details           Attempts to read from a file called "control_cal_coeffs.txt" and then create a self variable for it.
            @param option      Argument is a None brought in from main that is used in the if block that checks for the "control_cal_coeffs.txt" file in the directory.
        '''                     
        
        self.calibrationFile = "control_cal_coeffs.txt"
        
        if (self.calibrationFile in os.listdir() and option != 'm'):
            self.matrices = self.initializeGainMatrices()            
            
        else:
            print ('Calibration file does not exist. Using standard values')
            v = float(.0001)
            
            kX_matrix = gainVector.GainVector("Kx",v, v, v, v)
            kY_matrix = gainVector.GainVector("Ky",v, v, v, v)
            self.matrices = [kX_matrix, kY_matrix]
            
        print('Kx : {0}'.format(self.matrices[0].getVector()))
        print('Ky : {0}'.format(self.matrices[1].getVector()))
        
        self.Tx = 0
        self.Ty = 0
        
        # R and Kt come from the motor datasheet.
        self.R = 2.21         # resistance of the motor in ohms
        self.Kt = .0138       # gain of the torque Nm/A
        self.Vdc = 12.2       # 12V power supply
        
        self.isActive = False
    
    def active(self):
        ''' @brief          Function that returns the self.isActive variable as a boolean value.
            @details           
            @return            Returns the self.isActive in boolean to assist in the activation of the closed loop controller through the task user.
        '''
        return self.isActive
    
    def toggleActive(self):
        ''' @brief         Sets the self.isActive variable
            @details
            @return           Returns the boolean opposite of what the self.isActive is currently.
        '''
        self.isActive = not self.isActive
    
    def initializeGainMatrices(self):
        ''' @brief         Initializes the gain matrices for the motors.
            @details          Attempts to read from an already existing file called "control_cal_coeffs.txt" by calling the self.calibrationFile.
            @details          If this file is present, it sets the Kx and Ky matrices by reading the values from the txt file and appending the Kx and Ky arrays.
            @details          If this file is not present, the matrices will be set to values of zero as that's how they are intialized.
            @return           Returs a marix that includes both the Kx and Ky matrices.
        '''
        n = '\n'
        r = '\r'
        temp = [] 
        f = open (self.calibrationFile, "r")
        print('Attempting to read from \"{0}\"...'.format(self.calibrationFile),end='')
        for line in f:
            line.strip(n)
            line.strip(r)
            temp.append(float(line))
            
        f.close()
        
        for t in temp:
            print(t)
            
        kX_matrix = gainVector.GainVector("Kx", temp[0], temp[1], temp[2], temp[3])
        kY_matrix = gainVector.GainVector("Ky", temp[4], temp[5], temp[6], temp[7])
            
        return [kX_matrix, kY_matrix]       
    
    def computeTorques(self, positionMatrices):
        ''' @brief                  Computes the torque required for the motors to properly balance the board.
            @details                    Includes a for loop that indexes the Kx and Ky gain matrices then individually multiplies those K values by their associated value from the kinematic matrix.
            @details                    The loop then sums each of those combined K and kinematic numbers to get a singular torque value.
            @details                    The loop runs and then the self.Tx and self.Ty variables are updated per the summing through the for loop.
            @param positionMatrices     This argument includes the most recently updated x and y kinematic vectors. This is called in the balanceSystem function inside the task_closed_loop_controller.py file. 
        '''
        t = 0.0
        
        index = 0
        
        for i in self.matrices[0].getVector():
            temp = i * float(positionMatrices[0][index])
            t+=temp
            index +=1
        
        self.Tx = t
        t = 0.0
        index = 0
        
        for i in self.matrices[1].getVector():
            temp = i * float(positionMatrices[1][index])
            t+=temp
            index +=1
            
        self.Ty = t
        
        # if (axisType == 'x'):
            
        #     while (index < 4):
        #         temp = -1 * (float(self.matrices[0].getVector()[index]) * float(positionMatrix[index]))
        #         t += temp
        #         # print (t)
        #         index +=1
                
        #     self.Tx = t
        # else:
        #     while (index < 4):
        #         temp = -1 * (float(self.matrices[1].getVector()[index]) * float(positionMatrix[index]))
        #         t += temp
        #         # print (t)
        #         index +=1
            
        #     self.Ty = t

    def getGainMatrices(self):
        ''' @brief         Returns the gain matrices for the motors.
            @details          
            @return           Returns a Kx or Ky matrix depending one whether index [0] or index [1] is called.
        '''
        return self.matrices
    
    def displayGainMatrix(self):
        ''' @brief         Displays the gain matrices for the motors.
            @details          
        '''
        print('Kx: {0}'.format(self.matrices[0].getVector()))
        print('Ky: {0}\n'.format(self.matrices[1].getVector()))
    
    def modifyGainMatrix(self, matrix, ID):
        ''' @brief                  Modifies the gain matrices.
            @details                    Can modify the Kx and Ky matrices individually through an if and elif block.
            @param matrix               This argument brings in the previous adjusted gain matrix from the task_closed_loop_controller that has been altered by the user in the userinterface when tuning the gain values so that it can be updated properly.
            @param ID                   This refers to either the Kx or Ky gain matrix to make sure the correct matrix is modified.   
        '''
        if (ID == "Kx"):
            self.matrices[0].modifyVectorComponent(matrix)
        elif (ID == "Ky"):
            self.matrices[1].modifyVectorComponent(matrix)
        
    def saveGainMatrices(self):
        ''' @brief                   Saves the gain matrices to a txt file.
            @details                    Uses a for loop to write in the updated and modified gains that were adjusted in the user interface so they can be read when the code is ran again.
        '''
        
        print('Saving Kx = {0} and Ky = {1} to {2}...'.format(self.matrices[0].getVector(),
                                                              self.matrices[1].getVector(), self.calibrationFile),end='')
        f = open(self.calibrationFile, "w")
        
        for m in self.matrices:
            for i in m.getVector():
                f.write("{0}\n".format(i))
        
        f.close()
        print('done\n')
        
    def getTx(self):
        ''' @brief                   Returns the Tx self variable.
            @details                    Tx refers to the torque for altering panel orientation with the Kx values.
            @return                     Returns self.Tx 
        '''
        return self.Tx
            
    def getTy(self):
        ''' @brief                   Returns the Ty self variable.
            @details                    Ty refers to the torque for altering panel orientation with the Ky values.
            @return                     Returns self.Ty
        '''
        return self.Ty
    
    def getDutyVector(self):
        ''' @brief                   Returns a duty cycle matrix that includes the duty cycle for each motor.
            @details                    The equations are computed in SI units and use the motor constants initialized in the init function.
            @return                     Returns the duty for both motors in a matrix.
        '''
        return ((-1 * self.Tx * ((.25 * 100 * self.R)/(self.Kt*self.Vdc))), 
                (-1 * self.Ty * ((.25 * 100 * self.R)/(self.Kt*self.Vdc))))
    