# -*- coding: utf-8 -*-
'''
@file:      gainVector.py
@brief      Stores a vector of gain values for motor control.
@details    Storing these gain values in a matrix makes them easily modifiable when 
            trying to tune our duty cycle. This file allows us to easily modify each
            gain value just once and it will update everywhere to be used in
            calculating our duty cycles and matrix equations for torque. 

@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      December 8th, 2021
'''
class GainVector:
    
    def __init__(self, ID, k1, k2, k3, k4):
        ''' @brief      
            @details
            @param ID Specifies whether we are using the gain values for x and y.
            @param k1 The first gain affects the position of the ball
            @param k2 The second gain value affects the theta angle of tilt
            @param k3 The third gain value affects the velocity of the ball
            @param k4 The fourth gain value affects the angular velocity of the platform
        '''
        self.ID = ID
        
        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.k4 = k4
        
        self.vector = (self.k1,self.k2,self.k3,self.k4)
        
    def getVectorID(self):
        ''' @brief Gets the ID number for the gains being used.
        '''
        return self.ID
    
    def getVector(self):
        ''' @brief Gets the kinematic vector being used.
        '''
        return self.vector
        
    def modifyVectorComponent(self, matrix):
        ''' @brief   Modifies the matrix used to calculate torque.
            @details Modifies the gain values in the the matrix algebra when
                     calculating our torque values
        '''
        self.vector = (self.vector[0] + matrix[0], self.vector[1] + matrix[1],
                       self.vector[2] + matrix[2], self.vector[3] + matrix[3])