# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 17:10:15 2021

@author: jason
"""

class KinematicVector:
    
    def __init__(self):
        self.x = -1
        self.y = -1
        self.xDot = -1
        self.yDot = -1
        
        self.thetaX = -1
        self.thetaY = -1
        self.omegaX = -1
        self.omegaY = -1
        
        self.errThetaX = float(0)
        self.errThetaY = float(0)
        self.errOmegaX = float(0)
        self.errOmegaY = float(0)
        
    def getX(self):
        return self.x
    
    def getY(self):
        return self.y
    
    def getXDot(self):
        return self.xDot
    
    def getYDot(self):
        return self.yDot
    
    def getThetaX(self):
        return self.thetaX
    
    def getThetaY(self):
        return self.thetaY
    
    def getOmegaX(self):
        return self.omegaX
    
    def getOmegaY(self):
        return self.omegaY
    
    def setX(self, value):
        self.x = value
        
    def setY(self, value):
        self.y = value
        
    def setXDot(self, value):
        self.xDot = value
        
    def setYDot(self, value):
        self.yDot = value
    
    def setThetaX(self, value):
        value -= self.errThetaX
        self.thetaX = value
    
    def setThetaY(self, value):
        value -= self.errThetaY
        self.thetaY = value
        
    def setOmegaX(self, value):
        value -= self.errOmegaX
        self.omegaX = value
        
    def setOmegaY(self, value):
        value -= self.errOmegaY
        self.omegaY = value
        
    def setErrThetaX(self, value):
        self.errThetaX = value
        
    def setErrThetaY(self, value):
        self.errThetaY = value
        
    def setErrOmegaX(self, value):
        self.errOmegaX = value
        
    def setErrOmegaY(self, value):
        self.errOmegaY = value