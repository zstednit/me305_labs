# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 11:24:04 2021

@author: jason
"""
import os
import utime, pyb, gc
from micropython import const

S0_init = const(0)
S1_calibrateSensor = const(1)

class Task_IMU:
    
    def __init__(self, taskID, imu, period, imu_share):
        self.imu = imu
        self.taskID = taskID
        self.period = period
        self.imu_share = imu_share
        
        self.ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_init
        ## The number of runs of the state machine
        # self.runs = 0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
    def run(self):
        
        gc.collect()
        current_time = utime.ticks_us()
        
        action = self.imu_share.read()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0_init:
                
                if (action == 0):
                    self.transition_to(S1_calibrateSensor)
                    self.calibrate()
                # displaying the menu and then advancing to state 1 of the
                # task_user FSM
                    self.imu_share.write(None)
                    self.transition_to(S0_init)
                
                    
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            # self.runs += 1
    
    def calibrate(self):
        data = self.imu.get_calibration_data()
        print("Calibration Value: " + str(data))
        #print('test calibration loop runs')
        utime.sleep(.25)
        
        if(data[0] + data[1] + data[2] + data[3] == 12):     # checking to see if calibration is complete by confirmng all four 
            self.imu.setCalibrationStatus(True)
            print('Calibration completed \n'
                  'Calibration Status: '+str(self.imu.get_calibration_coefficient()))
    
    def getIMU(self):
        return self.imu
    
    
        
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        # if (self.dbg):
        #     print('{:}: S{:}->S{:}'.format(self.taskID ,self.state,new_state))
        self.state = new_state