"""
@file       task_panel.py
@brief      Responsible for running the tasks related to reading from our touch panel
@details    

@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      December 8th, 2021
"""
import os
import panelDriver
from ulab import numpy
import utime

class Task_Panel:
    ''' @brief          Task_Panel class object.
        @details
    '''
    
    def __init__(self, panelDriverObject):
        ''' @brief          Constructor for Task_Panel class
            @details
            @param
        '''
        self.panelDriverObject = panelDriverObject
        
    def run(self, kinVector):
        self.kinVector = kinVector
        while (True):
            if (self.panelDriverObject.panelTouch()):
                # Read x y z coordinates
                xCoordinate, yCoordinate, zCoordinate = self.panelDriverObject.readXYZ()
                # Read the alpha beta filter values.
                x_filter, x_dot_filter, y_filter, y_dot_filter, z = self.panelDriverObject.filtering(utime.ticks_us(50))
                x1 = xCoordinate
                x_dot1 = x_dot_filter
                y1 = yCoordinate
                y_dot1 = y_dot_filter
                # print('The panel is being touched: The X coordinate is {:} and the Y coordinate is {:}'.format(xCoordinate, yCoordinate))
                # Assign the values read from above to the kinematic vector to be
                # used in the main file. 
                self.kinVector.setX(x1)
                self.kinVector.setY(y1)
                self.kinVector.setXDot(x_dot1)
                self.kinVector.setYDot(y_dot1)
            else:
                print("Touch the panel")
    
    def calibrate(self):
        filename = "RT_cal_coeffs.txt"

        if filename in os.listdir():
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
        else:
            tapPoints = numpy.array([[-80, -40], [80, -40], [-80, 40], [80, 40], [0, 0]])
            uncalPoints = numpy.zeros(5,3)
            for i in range(5):
                print("Begin manual calibration...")
                print("Press point ({}, {})".format(tapPoints[i,0], tapPoints[i,1]))
                while (True):
                    try:
                        if (self.panelDriverObject.panelTouch()):
                            xCoordinate, yCoordinate, zCoordinate = self.panelDriverObject.readXYZ()
                            uncalPoints[i,0] = xCoordinate
                            uncalPoints[i,1] = yCoordinate
                            uncalPoints[i,2] = 1
                            break
                    except:
                        KeyboardInterrupt()
                        
            with open(filename, 'w+') as f:
                # calibrate manually, write coeff. to file
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = panelDriver.calibSensorOnPanel(uncalPoints)
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")