# -*- coding: utf-8 -*-
''' @file                       main.py
    @brief                      The main function to call a driver for the 9-axis BNO055 IMU
    @details
    @author                     Jason Davis
    @author                     Conor Fraser
    @date                       October 11, 2021
'''
import pyb, utime, shares
import DRV8847, closed_loop_controller, panel, BNO055
import task_user, task_motor, task_motorDriver, task_closed_loop_controller, task_IMU, task_panel
from pyb import I2C
    
print('Executing system setup functions...\n')

print('Initializing system components...')  
#creating motor driver / motor objects
print('    DRV8847...', end = '')
motorDriver = DRV8847.DRV8847(pyb.Timer(3, freq=20000))
print('done')

print('    Motors...', end = '')
input1 = pyb.Pin.cpu.B4
input2 = pyb.Pin.cpu.B5
input3 = pyb.Pin.cpu.B0
input4 = pyb.Pin.cpu.B1
motor_A = motorDriver.motor(input1, input2, 1, 2, "MOTOR A")
motor_B = motorDriver.motor(input3, input4, 3, 4, "MOTOR B")
print('done')

print('    Closed loop controller...', end = '')
controller = closed_loop_controller.ClosedLoop(1, 100, True)
print('done')

print('    BNO055...', end = '')
print('establishing I2C communication...', end = '')
i2c = I2C(1, I2C.MASTER)                    # intialize i2c object 
i2c.init(I2C.MASTER, baudrate = 500000)     # calls the init function
# time delay to let I2C port to activate
utime.sleep(.5)     
imu = BNO055.BNO055(0x28, i2c, False)
print('done')

print('    Touch panel...', end = '')
Xp = pyb.Pin.board.PA7
Xm = pyb.Pin.board.PA1
Yp = pyb.Pin.board.PA6
Ym = pyb.Pin.board.PA0
xDim = 176
yDim = 100
inputPins = [Xp, Xm, Yp, Ym]    
panelDims = [xDim, yDim]
touchPanel = panel.Panel(inputPins, panelDims)
print('done')

devices = [motorDriver, motor_A, motor_B, controller, imu, touchPanel]
print('System components successfully initialized!\n')

print('Initializing shares...', end = '')
#creating relevant share objects for the motors
motor_share = shares.Share()
output_share = shares.Share(0)
delta_share = shares.Share(0)
controller_share = shares.Share()
imu_share = shares.Share(0)
panel_share = shares.Share()
shares = [motor_share, delta_share, output_share, controller_share, imu_share, panel_share]
print('done')

print('Building user interface...', end = '')
ui = task_user.Task_User('USER', 40000, shares, dbg=False)
print('done')

print('Initializing task objects...', end = '')
task_motorDriver = task_motorDriver.Task_motorDriver('MOTOR DRIVER', motorDriver, [motor_A, motor_B] , motor_share, 10000, False)
task_motorA = task_motor.Task_Motor('MOTOR_A', 10000, motor_A, motor_share, output_share)
task_motorB = task_motor.Task_Motor('MOTOR_B', 10000, motor_B, motor_share, output_share)
task_controller = task_closed_loop_controller.Task_Closed_Loop_Controller('CONTROLLER', devices, shares, False)
task_imu = task_IMU.Task_IMU('IMU', imu, 1000, imu_share)
task_panel = task_panel.Task_Panel('PANEL', touchPanel, 1000, panel_share)

taskList = [ui, task_motorDriver, task_motorA, task_motorB, task_controller, task_imu, task_panel]
print('done\n')

print('System Ready\n')

systemCalibrated = task_imu.getIMU().getCalibrationStatus()
input('Press [enter] to begin IMU calibration')

while (True):
    try:
        while (not systemCalibrated):
            task_imu.calibrate()
            systemCalibrated = task_imu.getIMU().getCalibrationStatus()
            
        for task in taskList:
            task.run()
    except KeyboardInterrupt:
        break
print('\n*** Program ending, have a nice day! ***\n')

# action items
# sleep function wrong?     

# if __name__ == '__main__':  
#     print('Program Start')
    
#     i2c = I2C(1, I2C.MASTER)                    # intialize i2c object 
#     i2c.init(I2C.MASTER, baudrate = 500000)     # calls the init function
    
#     utime.sleep(.5)     # time delay to let I2C port to activate
    
#     IMUDriver = BNO055.BNO055(0x28, i2c)
    
#     calibration = False
    
#     while (calibration == False):
#         try:
#             status = IMUDriver.get_calibration_status()
#             print("Calibration Value: " + str(status))
#             #print('test calibration loop runs')
#             utime.sleep(.25)
            
#             if(status[0] + status[1] + status[2] + status[3] == 12):     # checking to see if calibration is complete by confirmng all four 
#                 calibration = True
#                 print('Calibration completed \n'
#                       'Calibration Status: '+str(IMUDriver.get_calibration_coefficient()))
#                 while (True):
#                     try:
#                         print('Euler: [Heading, Roll, Pitch] '+str(IMUDriver.read_euler_angles()))
#                         print('Angular Velocity: '+str(IMUDriver.read_angular_velocity()))
#                         print('\n')
#                         utime.sleep(.25)
                              
#                     except KeyboardInterrupt:
#                         break
#         except KeyboardInterrupt:
#             break
#     print('End of Program')