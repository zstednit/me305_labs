"""
Created on Thu Oct 21 16:50:06 2021

@author: jason
"""
import pyb, utime

class DRV8847:
    
    def __init__(self, motorTimer):       
        
        self.motorTimer = motorTimer
        
        ## Both nSleep and nFault are pulled high to enable and low to disable
        # self.nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        # self.nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN) 
        # self.button = pyb.Pin (pyb.Pin.cpu.C13)
        # # self.faultInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING,
        # #                                  pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        # self.buttonInt = pyb.ExtInt(self.button, mode=pyb.ExtInt.IRQ_FALLING, 
        #                         pull=pyb.Pin.PULL_NONE, callback=self.buttonPressed)
        
        # forces the motor to begin in a disabled state
        # self.nSLEEP.low()
        #modifications for term project
        # self.faultInt.disable()                # Disable fault interrupt
        
        
    # def enable (self):
    #     # self.faultInt.disable()                # Disable fault interrupt
    #     # self.nSLEEP.high()                      # Re-enable the motor driver
    #     # utime.sleep_us(25)                      # Wait for the fault pin to return high
    #     # self.faultInt.enable()                 # Re-enable the fault interrupt
    
    # def disable (self):
    #     # self.nSLEEP.low()
    #     print('Motors disabled')
        
        
    ## There are several fault conditions that may damage the motor / driver
    ## This function must disable the motor when a fault condition occurs
    ## Reference the button interrupt code used in lab 1  
# =============================================================================
#     5 Fault conditions possible:
#       undervoltage          *we're not running off of a battery pack, so not an issue
#       overcurrent
#       short-circuit         fault triggers appropriately
#       open-load             
#       overtemperature       not really running long enough to worry about 
# =============================================================================
    # def fault_cb (self, IRQ_src):
    #     print('  *** FAULT DETECTED! SUSPENDING DRV8847 HARDWARE OPERATION ***')
    #     self.disable()
        
    
    # def buttonPressed(self, IRQ_src):
    #     # print('  *** FAULT DETECTED! SUSPENDING DRV8847 HARDWARE OPERATION ***')
    #     self.disable()
        
    
    # def clearFaultCondition(self):
    #     self.enable()
    #     # print('   *** FAULT CONDITION CLEARED ***')
        
    def motor (self, inputA, inputB, channelA, channelB, motorID):
        return Motor(inputA, inputB, self.motorTimer, channelA, channelB, motorID)
    
class Motor:
    
    def __init__ (self, inputA, inputB, motorTimer, channelA, channelB, motorID):    
        self.inputA = inputA
        self.inputB = inputB
               
        self.motorTimer = motorTimer
        # establish the motor timer channels here
        
        self.channelA = channelA
        self.channelB = channelB
        self.motorID = motorID
        
        self.channel1 = self.motorTimer.channel(self.channelA, pyb.Timer.PWM, pin=inputA)
        self.channel2 = self.motorTimer.channel(self.channelB, pyb.Timer.PWM, pin=inputB)
        
        self.duty = 0
        
        self.isRunning = False
        self.direction = 0
            
    def getMotorID(self):
        return self.motorID
    
    ##also sets the direction
    def setDuty(self, duty):
        
        if (duty > 0):
            self.duty = duty
            self.direction = 1
            #set the "reverse" channel to zero first
            self.channel2.pulse_width_percent(0)
            self.channel1.pulse_width_percent(duty)
            self.isRunning = True 
            
        elif (duty < 0):
            duty *= -1
            self.duty = duty
            self.direction = -1
            #set the "forward channel to zero first
            self.channel1.pulse_width_percent(0)
            self.channel2.pulse_width_percent(duty)
            self.isRunning = True
            
        elif (duty == 0):
            self.duty = duty
            self.direction = 0
            self.brake()
            # print("{0} is stationary\n".format(self.motorID))
            self.isRunning = False
                
    def getDuty(self):
        return self.duty

    def getDirection(self):
        return self.direction
    
    def getRunState(self):
        return self.isRunning
    
    def toggleRunState(self):
        self.isRunning = not(self.isRunning)  
        
    def coast(self):
        self.channel1.pulse_width_percent(0)
        self.channel2.pulse_width_percent(0)
    
    def brake(self):
        self.channel1.pulse_width_percent(100)
        self.channel2.pulse_width_percent(100)
        
if __name__ == '__main__' :
    
    #defining motor inputs
    input1 = pyb.Pin.cpu.B4
    input2 = pyb.Pin.cpu.B5
    input3 = pyb.Pin.cpu.B0
    input4 = pyb.Pin.cpu.B1
    
    #creating motor driver / motor objects
    motorDriver = DRV8847(pyb.Timer(3, freq = 20000))
    m1 = motorDriver.motor(input1, input2, 1, 2, "Motor A")
    m2 = motorDriver.motor(input3, input4, 3, 4, "Motor B")
    