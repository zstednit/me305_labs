'''
@file Homework2and3.py
@brief Compiles the homework 2 and 3 assignments. 

@author Zachary Stednitz

@date 12/8/2021

@section sec_overview Overview

These are the hand calculations for the term project. 
    
@image html "HW2pg1.jpg" width=50%
@image html "HW2pg2.jpg" width=50%
@image html "HW2pg3.jpg" width=50%
@image html "HW2pg4.jpg" width=50%
@image html "HW2pg5.jpg" width=50%
@image html "HW2pg6.jpg" width=50%
@image html "HW2pg7.jpg" width=50%
@image html "HW2pg8.jpg" width=50%
@image html "HW2pg9.jpg" width=50%

To see the simulation Matlab live script, click here:
    <A HREF="Homework3.html">Matlab Live Script for Term Project</A>

'''