'''
@page "Lab 0: Fibonacci"

@author Zachary Stednitz

@date 12/8/2021

@section sec_overview Overview

This lab was focused around getting familiar with the Python language and 
also the IDE that we are working with, Spyder. The program would prompt the user
to input an index, and the program would calculate the resulting Fibonacci value.
It avoids using recursion to improve the speed of the program and instead uses 
an approach that stores previously calculated values in a list so they can be
accessed later. 

To see the code implemenation, refer to this repository: 
    https://bitbucket.org/zstednit/me305_labs/src/master/
    
To see documentation for this file, visit zs_fibonacci.py \ref

'''
