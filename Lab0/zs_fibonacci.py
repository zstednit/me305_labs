# -*- coding: utf-8 -*-
"""
@file:   zs_fibonacci.py
@brief   Calculate fibonacci numbers at a specified index
@author: Zachary Stednitz
@date:   12/8/2021

"""

'''
There must be a docstring at the beginning of a Python source file with an
\@file (filename) tag in it!
'''

def fib(idx):
    '''
    @brief This function calculates a fibonacci number at a specified index.
    @param idx An integer specifying the index of the desired Fibonacci number
    '''
    #Create an array of Fibonacci numbers to avoid using recursion, this is
    #the bottom up approach.
    
    f_0 = 0     #The first 2 fibonacci numbers are always the same.
    f_1 = 1
    fibnum = 0  #Assign the variable fibnum so it can be reassigned later
    
    for n in range(1,idx):
        if idx == 0:
            fibnum = f_0 #First index of 0 is always zero
            break
        elif idx == 1:
            fibnum = f_1 #The index at 1 is always 1
            break
        else:
            fibnum = f_1+f_0 #fibnum is reassigned to be the fibonacci algorithm
            f_0 = f_1        #reassign f_0 and f_1 to be the next step up. 
            f_1 = fibnum 
    return(fibnum) #return our final fibonacci number
            
if __name__ == '__main__':
    while True:
        #Take the user input index
        idx = input('Please enter an index to find the Fibonacci number: ')
        try:
            my_int = int(idx) #Convert the user input to an integer from string
        except:
            print('Input must be an integer, not a string')
            #If the input cannot be converted to an integer, then display text
            #to help guide the user
            continue
        else:
            if my_int == 0:
                fibonacci = 0
                print('Fibonacci number at '
                      'index {:} is {:}.'.format(my_int,fibonacci))
                cont = input('Press enter to input another index or Q to quit: ')
                if cont == 'q' or cont == 'Q':
                    # If the user presses q, the program quits
                    break
                else:
                    #If the user hits enter, then the program prompts them again.
                    continue
            elif my_int == 1:
                fibonacci = 1
                print('Fibonacci number at '
                      'index {:} is {:}.'.format(my_int,fibonacci))
                cont = input('Press enter to input another index or Q to quit: ')
                if cont == 'q' or cont == 'Q':
                    # If the user presses q, the program quits
                    break
                else:
                    #If the user hits enter, then the program prompts them again.
                    continue
            elif my_int < 0:
                #Tell the user some parameters for entering their index
                print('Index cannot be negative')
                continue
            else:
                #Call the fib(idx) function
                fibonacci = fib(my_int)
                print('Fibonacci number at '
                      'index {:} is {:}.'.format(my_int,fibonacci))
                #Display user input choice to continue or end the loop
                cont = input('Press enter to input another index or Q to quit: ')
                if cont == 'q' or cont == 'Q':
                    # If the user presses q, the program quits
                    break
                else:
                    #If the user hits enter, then the program prompts them again.
                    continue
                    