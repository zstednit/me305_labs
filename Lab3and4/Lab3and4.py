'''
@page "Labs 3 and 4 Motor Control"

@author Zachary Stednitz
@author Jason Davis
@author Conor Fraser
@author Solie Grantham

@date 12/8/2021

@section sec_overview Overview

@image html "Delta_v_Time.png" width=50%
@image html "Position_v_Time.png" width=50%

This lab used PWM to control the duty cycle of two different motors and then
used a closed loop proportional controller to set different duty cycles in order
to match the real output velocity to the reference velocity of the motor.
While the code seen in the documentation is the most up to date versions of files
we have used after this lab had concluded, you can find the unmodified files
at this respository: \n
https://bitbucket.org/zstednit/me305_labs/src/master/Lab3and4/

To view documentation for the different files, visit: \n
    closed_loop_controller.py \ref \n
    DRV8847.py \ref \n
    encoder.py \ref \n
    main.py \ref \n
    shares.py \ref \n
    task_closed_loop_controller.py \ref \n
    task_encoder.py \ref \n
    task_motor.py \ref \n
    task_motorDriver.py \ref \n
     task_user.py \ref \n
    
'''
