# -*- coding: utf-8 -*-
"""
@file:      task_closed_loop_controller.py
@brief      Closed loop controller that corrects duty cycles for the motors
@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      November 17th, 2021

"""

import utime, pyb
from micropython import const

S0_init = const(0)
S1_closedLoop = const(1)

class Task_Closed_Loop_Controller:
    def __init__(self, taskID, devices, shares, dbg):
        ''' @brief      Initializes the closed loop controller task.
            @details    Utilizes device shares to get data from the other objects
                        in order to control the motor speed.
            @param      taskID The name of the task
            @param      devices A list of devices available to be called 
            @param      shares Shares object that enables data sharing between classes. 
            @param      dbg Optional debugging message will display if dbg flag is called
        '''
        self.taskID = taskID
        
        self.encoder1 = devices[0]
        self.encoder2 = devices[1]
        self.motorDriver = devices[2]
        self.motor1 = devices[3]
        self.motor2 = devices[4]
        self.controller = devices[5]
        
        self.encoder_share = shares[0]
        self.motor_share = shares[1]
        self.delta_share = shares[2]
        self.controller_share = shares[3]
        self.output_share = shares[4]
        
        self.period = 10000
        self.ser = pyb.USB_VCP()
        self.state = S0_init
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        self.dbg = dbg
        
        self.omega_ref = 0
        
    def run(self):
        ''' @brief      Updates duty cycle based on closed loop controller
            @details    Calls the controller update function to compare the desired 
                        and actual velocities of the motor and then collects user input
                        to change the controller gain value to tune the response.
        '''
        # self.controller_share.write('closedLoopMotor1')
        duty = self.controller.update(self.omega_ref, self.encoder1.getRadiansPerSecond())

        action = self.controller_share.read()
        # debugging only
        
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if (self.state == S0_init):
                
                if (action == 0):
                    self.transition_to(S1_closedLoop)
                    msg = 'Enter Kp value (value > 4 not recommended): '
                    Kp = self.collectBufferedInput(msg)
                    self.controller.set_Kp(Kp)
                    msg = '\nEnter desired motor speed in rad/s: '
                    self.omega_ref = self.collectBufferedInput(msg)
                    # duty = self.controller.update(self.omega_ref, self.encoder1.get_delta())
                    self.motor1.setDuty(duty)
                    print('\nMotor 1 control parameters successfully updated\n')
                    self.controller_share.write(None)
                    self.encoder_share.write('o')
                    print('Beginning encoder 1 data collection...')
                    self.transition_to(S0_init)

                if (action == 1):
                    self.transition_to(S1_closedLoop)
                    msg = 'Enter Kp value (value > 4 not recommended): '
                    Kp = self.collectBufferedInput(msg)
                    self.controller.set_Kp(Kp)
                    msg = '\nEnter desired motor speed in rad/s: '
                    omega_ref = self.collectBufferedInput(msg)
                    duty = self.controller.update(omega_ref, self.encoder2.get_delta())
                    self.motor2.setDuty(duty)
                    print('\nMotor 2 control parameters successfully updated\n')
                    self.controller_share.write(None)
                    self.encoder_share.write('O')
                    print('Beginning encoder 2 data collection...')
                    self.transition_to(S0_init)
            else:
                self.transition_to(S0_init)
            self.next_time = utime.ticks_add(self.next_time, self.period)
    
    def collectBufferedInput(self, msg):
        ''' @brief          Collects user input to modify the controller.
            @details        Collects user input from the VCP and performs logic 
                            to determine if the input in a digit and valid to 
                            modify the controller.
            @param          msg A message formatted from VCP serial input
        '''
        print('{0} '.format(msg), end = '')
        
        userInput = str(self.ser.read(2))
        if (len(userInput) > 4):
            t = userInput[2:4]
        else:
            t = userInput[2:3]
        temp = list([])
        
        # hitting the 'enter' key sends \r character to the VCP
        while (t != '\\r'):
            
            # append the information in the VCP to "duty" only if it is a digit
            if (t.isdigit()):
                # print('t is a digit')
                print ('{0}'.format(t), end = '')
                temp.append(t)
            
            userInput = str(self.ser.read(2))
            if (len(userInput) > 4):
                t = userInput[2:4]
            else:
                t = userInput[2:3]
            
            # NOTE: everytime we read values from the VCP, it then EMPTIES the VCP
            # We need to store the value of the VCP in a temp variable if we wish to use it later
        output = ''.join(map(str, temp))
        
        return output
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.taskID ,self.state,new_state))
        self.state = new_state