# -*- coding: utf-8 -*-
"""
@file:      main.py
@brief      Main program for running the motor and encoder tasks
@details    Creates encoder and motor objects, the shares for 
            sharing tasks between programs, and creating the tasks
            for each object. Runs the tasks when the user inputs them on the
            user interface
            
            \image html Delta_v_Time.png
            \image html Position_v_Time.png
            
@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      November 17th, 2021

"""
## LAB 4

import pyb
import encoder, DRV8847, closed_loop_controller, shares
import task_user, task_encoder, task_motor, task_motorDriver, task_closed_loop_controller
    
# instantiating our encoders
encoder_A = encoder.Encoder(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 4, ID="ENCODER A")
encoder_B = encoder.Encoder(pyb.Pin.board.PC6, pyb.Pin.board.PC7, 8, ID="ENCODER B")

# instantiating a share object for the task_encoder
encoder_share = shares.Share(0)

# a share object for the output
output_share = shares.Share(0)

delta_share = shares.Share(0)

input1 = pyb.Pin.cpu.B4
input2 = pyb.Pin.cpu.B5
input3 = pyb.Pin.cpu.B0
input4 = pyb.Pin.cpu.B1

#creating motor driver / motor objects
motorDriver = DRV8847.DRV8847(pyb.Timer(3, freq=20000))
motor_A = motorDriver.motor(input1, input2, 1, 2, "MOTOR A")
motor_B = motorDriver.motor(input3, input4, 3, 4, "MOTOR B")

listOfMotors = [motor_A, motor_B]

#creating a share object for the motors
motor_share = shares.Share()

controller_share = shares.Share()

controller = closed_loop_controller.ClosedLoop(1, 100, True)

#list of devices and shares to clean up the constructors
devices = [encoder_A, encoder_B, motorDriver, motor_A, motor_B, controller]
shares = [encoder_share, motor_share, delta_share, controller_share, delta_share]

# instantiating the user interface
# the ui will return a character representing the desired task and pass it to task_encoder
task_1 = task_user.Task_User('USER', 40000, encoder_share, output_share, delta_share, motor_share, controller_share, dbg=False)

# instantiating a task object for each task
task_2A = task_encoder.Task_Encoder('ENC_A', 10000, encoder_A, encoder_share, output_share, delta_share)
task_2B = task_encoder.Task_Encoder('ENC_B', 10000, encoder_B, encoder_share, output_share, delta_share)

task_3 = task_motorDriver.Task_motorDriver('MOTOR DRIVER', motorDriver, listOfMotors, motor_share, 10000, False)

task_4A = task_motor.Task_Motor('MOTOR_A', 10000, motor_A, motor_share, output_share)
task_4B = task_motor.Task_Motor('MOTOR_B', 10000, motor_B, motor_share, output_share)

task_5 = task_closed_loop_controller.Task_Closed_Loop_Controller('CONTROLLER', devices, shares, False)
# create a task list
taskList = [task_1, task_2A, task_2B, task_3, task_4A, task_4B, task_5]
# taskList = [task_1, task_3, task_4A, task_4B]
while (True):
    try:
        for task in taskList:
            task.run()
        
    except KeyboardInterrupt:
        break
print('\n*** Program ending, have a nice day! ***\n')
  