"""
@file       DRV8847.py
@brief      A motor driver class for the DRV8847 motor controller
@details    Objects of this class can be used to configure the DRV8847
            motor driver and to create one or more
            objects of the Motor class which can be used to perform motor 
            control.
                   
            Refer to the DRV8847 datasheet here:
            https://www.ti.com/lit/ds/symlink/drv8847.pdf
            
@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz

"""

import pyb, utime

class DRV8847:
    
    def __init__(self, motorTimer):      
        ''' @brief      Initializes and returns a DRV8847 object.
            @details    Creates the pin objects related to each motor and 
                        initializes timer channels on each pin to run PWM. 
                        Also creates a fault pin that calls back to a fault 
                        function that disables the motors when the motor triggers
                        an external interrupt.
            @param      motorTimer Controls timer to do arithmetic for motor duty cycle
        '''
        
        self.motorTimer = motorTimer
        
        ## Both nSleep and nFault are pulled high to enable and low to disable
        self.nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        self.nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN) 
        self.button = pyb.Pin (pyb.Pin.cpu.C13)
        self.faultInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING,
                                         pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        self.buttonInt = pyb.ExtInt(self.button, mode=pyb.ExtInt.IRQ_FALLING, 
                               pull=pyb.Pin.PULL_NONE, callback=self.buttonPressed)
        
        # forces the motor to begin in a disabled state
        self.nSLEEP.low()
        
    def enable (self):
        ''' @brief      Brings DRV8847 out of sleep mode.
            @details    Utilizes the nSLEEP pin created in the initializer to 
                        enable the motors and prepare them for operation. The function
                        utilizes a sleep timer to prevent the motor from giving 
                        a fault condition when the voltage spikes up after enabling
                        the motor for operation.  
        '''
        self.faultInt.disable()                # Disable fault interrupt
        self.nSLEEP.high()                      # Re-enable the motor driver
        utime.sleep_us(25)                      # Wait for the fault pin to return high
        self.faultInt.enable()                 # Re-enable the fault interrupt
    
    def disable (self):
        ''' @brief      Puts the DRV8847 in sleep mode.
            @details    Utilizes the nSLEEP pin created in the initializer to 
                        disable the motors. A disabled motor cannot be turned on 
                        and the user must enable the motors before setting a duty cycle
        '''
        self.nSLEEP.low()
        
    ## There are several fault conditions that may damage the motor / driver
    ## This function must disable the motor when a fault condition occurs
    ## Reference the button interrupt code used in lab 1  
# =============================================================================
#     5 Fault conditions possible:
#       undervoltage          *we're not running off of a battery pack, so not an issue
#       overcurrent
#       short-circuit         fault triggers appropriately
#       open-load             
#       overtemperature       not really running long enough to worry about 
# =============================================================================
    def fault_cb (self, IRQ_src):
        ''' @brief      Callback function to run on fault condition
            @param      IRQ_src The source of the interrupt request.
            @details    Notifies the user that a fault has been detected, and then 
                        when that happens disables the motor. User must re-enable 
                        motors to set duty cycles. 
        '''
        print('  *** FAULT DETECTED! SUSPENDING DRV8847 HARDWARE OPERATION ***')
        self.disable()
        
    
    def buttonPressed(self, IRQ_src):
        ''' @brief      Callback function to run on fault condition
            @param      IRQ_src The source of the interrupt request.
            @details    Notifies the user that a fault has been detected, and then 
                        when that happens disables the motor. User must re-enable 
                        motors to set duty cycles. 
        '''
        print('  *** FAULT DETECTED! SUSPENDING DRV8847 HARDWARE OPERATION ***')
        self.disable()
        
    
    def clearFaultCondition(self):
        ''' @brief      Clears the fault condition
            @details    CLears the fault condition by enabling the motors once again,
                        they're ready to be run again.
        '''
        self.enable()
        # print('   *** FAULT CONDITION CLEARED ***')
        
    def motor (self, inputA, inputB, channelA, channelB, motorID):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847
            @details    Returns a Motor object utilizing the motorID to identify which 
                        motor is being created
            @return     An object of class Motor
            @param      inputA The pin that the motor is connected to on the microcontroller
            @param      inputB The pin that the motor is connected to on the microcontroller
            @param      channelA The timer channel to be used with PWM to spin the motor in one direction
            @param      channelB The timer channel to be used with PWM to spin the motor the opposite direction
            @param      motorID Number to identify which motor is being modified. 
        '''
        return Motor(inputA, inputB, self.motorTimer, channelA, channelB, motorID)
    
class Motor:
    
    def __init__ (self, inputA, inputB, motorTimer, channelA, channelB, motorID):    
        ''' @brief      Initializes and returns a motor object associated with the DRV8847
            @details    Returns a Motor object utilizing the motorID to identify which 
                        motor is being created
            @return     An object of class Motor
            @param      inputA   The pin that the motor is connected to on the microcontroller
            @param      inputB   The pin that the motor is connected to on the microcontroller
            @param      motorTimer The timer that syncs the motor to the duty cycle.
            @param      channelA The timer channel to be used with PWM to spin the motor in one direction
            @param      channelB The timer channel to be used with PWM to spin the motor the opposite direction
            @param      motorID Number to identify which motor is being modified. 
        '''
        self.inputA = inputA
        self.inputB = inputB
               
        self.motorTimer = motorTimer
        # establish the motor timer channels here
        
        self.channelA = channelA
        self.channelB = channelB
        self.motorID = motorID
        
        self.channel1 = self.motorTimer.channel(self.channelA, pyb.Timer.PWM, pin=inputA)
        self.channel2 = self.motorTimer.channel(self.channelB, pyb.Timer.PWM, pin=inputB)
        
        self.duty = 0
        
        self.isRunning = False
        self.direction = 0
            
    def getMotorID(self):
        ''' @brief      Returns the motor ID value
        '''
        return self.motorID
    
    ##also sets the direction
    def setDuty(self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent to the motor
                        to the given level. Positive values cause motion in one
                        direction, negative values in the opposite direction.
            @param      duty A number holding the duty cycle of the PWM signal.
        '''
        if (duty > 0):
            self.duty = duty
            self.direction = 1
            #set the "reverse" channel to zero first
            self.channel2.pulse_width_percent(100-duty)
            self.channel1.pulse_width_percent(100)
            self.isRunning = True 
            
        elif (duty < 0):
            duty *= -1
            self.duty = duty
            self.direction = -1
            #set the "forward channel to zero first
            self.channel1.pulse_width_percent(100-duty)
            self.channel2.pulse_width_percent(100)
            self.isRunning = True
            
        elif (duty == 0):
            self.duty = duty
            self.direction = 0
            self.brake()
            # print("{0} is stationary\n".format(self.motorID))
            self.isRunning = False
                
    def getDuty(self):
        ''' @brief Returns the motor duty cycle
        '''
        return self.duty

    def getDirection(self):
        ''' @brief Returns the direction the motor is spinning
        '''
        return self.direction
    
    def getRunState(self):
        ''' @brief Returns the motor state it is currently running in
        '''
        return self.isRunning
    
    def toggleRunState(self):
        ''' @brief Tells us if the motor is running or not
            @details    Sets a boolean value to the isRunning function to determine 
                        the motor's run state
        '''
        
        self.isRunning = not(self.isRunning)  
        
    def coast(self):
        ''' @brief      Coasts the motor to a stop
            @details    Sets all PWM to 0 so that the rotor of the motor slows down
                        on its own with no force directly opposing it
        '''
        self.channel1.pulse_width_percent(0)
        self.channel2.pulse_width_percent(0)
    
    def brake(self):
        ''' @brief      Coasts the motor to a stop
            @details    Motor slows down by setting the PWM to 100 in the opposite 
                        directions so the motor is forced to stop.
        '''
        self.channel1.pulse_width_percent(100)
        self.channel2.pulse_width_percent(100)
        
if __name__ == '__main__' :
    
    #defining motor inputs
    input1 = pyb.Pin.cpu.B4
    input2 = pyb.Pin.cpu.B5
    input3 = pyb.Pin.cpu.B0
    input4 = pyb.Pin.cpu.B1
    
    #creating motor driver / motor objects
    motorDriver = DRV8847(pyb.Timer(3, freq = 20000))
    m1 = motorDriver.motor(input1, input2, 1, 2, "Motor A")
    m2 = motorDriver.motor(input3, input4, 3, 4, "Motor B")
    