# -*- coding: utf-8 -*-
''' @file                       task_user.py
    @brief                      A Task User that receives user input
    @details                    Contains a Class labelled Task_User which includes a variety of functions: 
                                init, run, displayMenu, formatNumeric, gatherData, haltDataGathering, and transition_to
    @author                     Zachary Stednitz
    @author                     Solie Grantham
    @author                     Jason Davis
    @author                     Conor Fraser  
    @date                       November 7, 2021
'''

import utime, pyb, gc
from micropython import const

## List of possible encoder states
S0_init = const(0)
S1_waitForInput = const(1)

class Task_User():
    '''@brief                       User interface task for cooperative multitasking example
       @details                     Checks for user input and writes to the correct shares that correspond to the task_encoder, task_motor, or task_motorDriver
    '''
    
    def __init__(self, taskID, period, encoder_share, output_share, delta_share, motor_share, dbg=False):
        '''@brief                   Constructor for the Task_User Class
           @details                 Instaniates our self variables
           @param taskID            The name of the task
           @param period            The period, in microseconds, between runs of the task
           @param encoder_share     Share object used to share data with the encoder
           @param output_share      Share object used to hold the position of the encoder then dump through the garabge collector to save memory                                    
           @param delta_share       Share object used to hold the delta value when collecting data
           @param motor_share       Share object used to share data with the motor
           @param dbg               A boolean flag used to enable or disable debug
                                    messages printed over the VCP
        '''
        ## The name of the task
        self.taskID = taskID
        ## The period (in us) of the task
        self.period = period
        ## A shares.Share object representing encoder
        self.encoder_share = encoder_share
        
        ## A shares.Share object representing the encoder output
        self.output_share = output_share
        
        self.delta_share = delta_share
        
        self.motor_share = motor_share
        
        ## A flag indicating if debugging print messages display
        self.dbg = dbg
        
        ## A serial port to use for user I/O
        ## A virtual "bucket" in which to dump user input
        ## Reading from 'ser' is like checking the bucket for any new input, only
        ## we don't halt the program in event that none is entered
        self.ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_init
        ## The number of runs of the state machine
        # self.runs = 0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        # the list where the times are stored
        self.times = []
        self.offsetTime = 0
        
        # the list where the positions are stored
        self.positions = []
        # the list where the deltas are stored
        self.deltas = []
        
        gc.enable()
        
    def run(self):
        '''@brief                   Runs one iteration of the FSM
           @details                 Includes two states. S0_init and S1_waitForInput
                                    S0 is a constructor state that displays the user interface instructions menu and then transitions to state 1
                                    S1 wait for user input that corresponds to actions such as return encoder position or enable motors
                                    Consists of an elif block with each possible user input action corresponding to a share write or a direct action
        '''
        gc.collect()
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0_init:
                
                # displaying the menu and then advancing to state 1 of the
                # task_user FSM
                self.displayMenu()
                self.transition_to(S1_waitForInput)       
                
        # we have to change the value in the VCM, otherwise the program will run in an inifinite loop
        # this is why we are reading letters and writing/sharing numbers
            elif self.state == S1_waitForInput:
                if (self.ser.any()):
                    char_in = self.ser.read(1).decode()
                    if (char_in == 'z'):
                        # passing the character to the task_encoder
                        self.encoder_share.write(1)
                            
                    elif (char_in == 'Z'):
                        self.encoder_share.write(6)
                        
                    elif(char_in == 'p'):
                        self.encoder_share.write(2)
                        
                    elif (char_in == 'P'):
                        self.encoder_share.write(7)
                        
                    elif (char_in == 'd'):
                        self.encoder_share.write(3)
                        
                    elif (char_in == 'D'):
                        self.encoder_share.write(8)
                        
                    elif (char_in == 'm'):
                        self.motor_share.write(9)
                    
                    elif (char_in == 'M'):
                        self.motor_share.write(10)
                        
                    elif (char_in == 'x' or char_in == 'X'):
                        self.motor_share.write(12)
                        
                    elif (char_in == 'y' or char_in == 'Y'):
                        self.motor_share.write(13)
                        
                    elif (char_in == 'c' or char_in == 'C'):
                        self.motor_share.write(0)
                    
                    elif (char_in == 'g'):
                        print ('Beginning encoder 1 data collection...')
                        self.start_time = current_time    
                        self.encoder_share.write(4)    
                            
                    elif (char_in == 'G'):
                        print ('Beginning encoder 2 data collection...')
                        self.start_time = current_time    
                        self.encoder_share.write(12)    
                            
                    elif (char_in == 's' or char_in == 'S'):
                        self.haltDataGathering()
                    
                    elif (char_in == 'e' or char_in == 'E'):
                        self.motor_share.write(11)
                        
                    elif (char_in == 'h' or char_in == 'H'):
                        self.displayMenu()
                    
                    # input validation done here
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
                
                #gathering data
                if (self.encoder_share.read() == 'k'):
                    self.gatherData(current_time, 4)
                  
                elif (self.encoder_share.read() == 'j'):
                    self.gatherData(current_time, 12)
                    
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            # self.runs += 1
    
    def displayMenu(self):
        '''@brief                   Displays the user input menu of commands
           @details                 This is called in S0_init   
        '''
        
        print()
        print('         Please select an option from the menu (case sensitive):')
        print ("+-------------------------------------------------------------------+")
        print ('| [z]     Zero encoder 1 position                                   |')
        print ('| [Z]     Zero encoder 2 position                                   |')
        print ('| [p]     Display encoder 1 position                                |')
        print ('| [P]     Display encoder 2 position                                |')
        print ('| [d]     Display delta for encoder 1                               |')
        print ('| [D]     Display delta for encoder 2                               |')
        print ('| [m]     Modify duty cycle for motor 1                             |')
        print ('| [M]     Modify duty cycle for motor 2                             |')
        print ('| [x/X]   Shortcut: Set both motors to max FWD                      |')
        print ('| [y/Y]   Shortcut: Set both motors to max REV                      |')
        print ('| [g]     Collect encoder 1 data for 30 seconds and display output  |')
        print ('| [G]     Collect encoder 2 data for 30 seconds and display output  |')
        print ('| [s/S]   End data collection prematurely                           |')
        print ('| [e/E]   Enable / Disable motors                                   |')
        print( "+-------------------------------------------------------------------+")
        print ('| [c/C]   Clear fault condition                                     |')
        print ('| [h/H]   Print this menu to the console                            |')        
        print ('| Ctrl+c  Terminate the program                                     |')
        print ("+-------------------------------------------------------------------+\n")
    
    ## this function cleans numeric output prior to printing it to the summary
    ## Rounds decimals to the two places and appends trailing zeros as necessary
    def formatNumeric(self, value, numPlaces):
        '''@brief                   Formats the user input to rounded decimal number
           @details                 Reads the pre and post decimal point portions of the user input and accordling rounds each if needed
                                    It then reconnects the pre decimal point portion and post decimal point portion to return a rounded value
           @param value             Value is pulled from the user input and is then rounded by the formatNumeric function
           @param numPlaces         Number of places of the input value   
           @return                  Returns a rounded user input value to the thousandths place      
        '''
        value = str(value)
        charArray = [char for char in value]
        
        decimalIndex = -1
        decimal = []
        
        for index in range(len(charArray)):
            if (charArray[index] == '.'):
                decimalIndex = index
            if (decimalIndex >= 0):
                decimal.append(charArray[index])
        
        whole = value[0:decimalIndex]
        dec = ''
                
        if (len(decimal) == 1):
            decimal.append('0')
            decimal.append('0')
        elif (len(decimal) == 2):
            decimal.append('0')
        elif (len(decimal) > 3):
            hun = decimal[2]
            thou = decimal[3]
            
            if (int(thou) >= 5):
                hun = int(hun) + 1
            dec = ''.join(decimal[0:3])
        
        value = whole + dec
        
        return value
    
    def gatherData(self, current_time, code):
        '''@brief                   Gathers data from the encoder
           @details                 Grabs the time in seconds, position in radians, and delta in radians/second
           @param current_time      Start time of our data collection 
           @param code              Placeholder for the correct share number to write to
        '''
        # print('adding an entry')
        gc.collect()
        self.times.append((current_time - self.start_time)/1000000)
        self.positions.append(self.output_share.read())
        self.deltas.append(self.delta_share.read())
        self.output_share.write(None)
        self.delta_share.write(None)
        # print("time diff: {0}".format((utime.ticks_diff(current_time, self.start_time)/1000000)))
        if ((utime.ticks_diff(current_time, self.start_time)/1000000) < 30.00):
            self.encoder_share.write(code)
        else:
            # print('Time is up')
            # self.encoder_share.write('s')
            self.haltDataGathering()
    
    def haltDataGathering(self):
        '''@brief                   Stops the gathering of data
           @details                 Halts collection of time in seconds, position in radians, and delta in radians/second 
        '''
        gc.collect()
        print('************************** Data Output ****************************')
        print('-------------------------------------------------------------------')
        print('Time [s]        Position [rad]        Delta [rad/s]')
        
        #cycling through the output list to display it on the screen
        
        for i in range(0, len(self.times)):
            print("{0} ,          {1} ,                   {2}".format(self.formatNumeric(self.times[i], 2), self.positions[i], self.deltas[i]))
                            
        print('***End of data collection\n')
        self.encoder_share.write(None) # clear the encoder share
        self.output_share.write(None) # clear the output share
        
        # clearing the arrays
        self.times = [] 
        self.positions = [] 
        self.deltas = [] 
        gc.collect()
    
    def transition_to(self, new_state):
        '''@brief                   Transitions the FSM to a new state
           @details                 Optionally a debugging message can be printed
                                    if the dbg flag is set when the task object is created
           @param                   new_state The state to transition to
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.taskID ,self.state,new_state))
        self.state = new_state