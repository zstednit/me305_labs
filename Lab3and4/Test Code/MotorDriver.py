# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 16:19:26 2021

@author: zsted
"""

import pyb
import time

sleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
sleep.high()

pinPB4 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pinPB5 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
# Second motor
pinPB0 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
pinPB1 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)

tim3 = pyb.Timer(3, freq = 20000)
t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=pinPB4)
t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=pinPB5)
t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin=pinPB0)
t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin=pinPB1)

# Set pulse width percent.
t3ch1.pulse_width_percent(100)
t3ch2.pulse_width_percent(0)
t3ch3.pulse_width_percent(100)
t3ch4.pulse_width_percent(0)