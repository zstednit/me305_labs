# -*- coding: utf-8 -*-
"""
@author: Zachary Stednitz
@author: Solie Grantham

@file     Lab1Main.py
@brief    Implementation of blinking LED patterns on the Nucleo
@details  This file will be used to demonstrate different LED light patterns 
          through the use of a Finite State Machine. The user will control which
          light pattern is seen and when the program starts by pressing the blue 
          button the Nucleo. The 3 LED light patterns are 
              1. Square Wave
              2. Sin Wave
              3. Sawtooth Wave
              
          When the user starts the Nucleo, this code will run automatically and 
          inform the user of how to use it. Then it will endlessly cycle through 
          the different light patterns when the Blue button is pressed. 
          \image html Lab_1_FSM.jpg
          
          Video of the LED patterns here:
              
          https://cpslo-my.sharepoint.com/:v:/g/personal/zstednit_calpoly_edu/Ec8iUv7Y1NtEh4cJJwvQ93gBwIAnNyi5rlNwlxPDAOhUxA?e=s80r80
"""

import pyb
import utime
import math

def onButtonPressFCN(IRQ_src):
    '''
    @brief    Function that detects button push
    @details  When this function is called later in the ButtonInt interrupt,
              the global variable buttonPressed will be assigned a value of true
              and indicates that the user has pressed the button.
    @param    IRQ_src is a condition of the button interrupt later used.
    '''
    global buttonPressed
    buttonPressed = True
    
def reset_Timer():
    '''
    @brief    Function resets our timer to a new reference point. 
    @details  When this function is called, the utime.ticks_ms() sets the start
              time to be used for reference in the later update_Timer() function
    @param    This function does not take any parameters since it is essentially 
              just setting a marker for the global variable startTime
    '''
    global startTime
    startTime = utime.ticks_ms()
    
def update_Timer():
    '''
    @brief    Function updates our timer duration while light patterns are displayed 
    @details  When this function is called, the utime.ticks_ms() sets a stop
              time to be used in the calculations for the duration variable. The
              duration variable is the main global variable used later on that 
              sets up the LED pattern periods and brightness.
    @param    This function does not take any parameters since it is essentially 
              just calculating the duration of the blink everytime update_Timer()
              is called. 
    '''
    stopTime = utime.ticks_ms()
    global duration
    duration = utime.ticks_diff(stopTime, startTime)
    
def update_SQW(duration):
    '''
    @brief    Function updates the Square Wave pattern brightness on the LED.
    @details  When this function is called, it returns a value between 0 and 100
              that can be used as % percent brightness by the LED. For the first half
              of the square wave, the brightness is at 100% and then 0% for the second half,
              so the LED is just blinking between two extreme values for brightness. 
    @param    This function takes the duration variable as a parameter as a way to 
              regulate the square wave's period.'
    '''
    return 100*(duration % 1000 < 500) 
    #Use 1000 and 500 as the conditions because we are in milliseconds

def update_SINW(duration):
    '''
    @brief   Function updates the Sine Wave pattern brightness on the LED.
    @details When this function is called it returns a value of the sine function
             as a function of the duration. As the duration changes, the sine wave
             will cycle through a full rotation. The wave is also shifted upwards 
             0.5 and given an amplitude of 100 to display the appropriate brightness.
    @param   This function takes the duration variable as a parameter to regulate
             the size of the sine wave's period.'

    '''
    return 100*(0.5+math.sin(2*math.pi*1/10000*duration))

def update_STW(duration):
    '''
    @brief   Function updates the Sawtooth Wave pattern brightness on the LED.
    @details When this function is called it returns a value of the duration in
             milliseconds after being divided by 10 to return the appropriate 
             magnitude of brightness.
    @param   This function takes the duration variable as a parameter to regulate 
             the size of the sawtooth wave's period.'
    '''
    return (duration % 1000)/10
    #Use 1000 for the modulus because we are in milliseconds. Divide by 10 to make
    #the value returned a number from 0 to 100%. 
    
#All functions should be defined before the if __name__ block below
if __name__ == '__main__':
    ## The next state to run as the FSM iterates
    state = 0
    #The FSM always starts in state 0
    buttonPressed = False
    initialState = 0 
    #Variable used later to prevent the code from 
    #printing the waiting for input message infinitely
    
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2,freq = 20000)
    t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)
    
    while(True):
        try:
            if(state == 0):
                #Run Welcome State code
                print('Press the blue button B1 to cycle through LED patterns')
                state = 1               
                #Always Transition to state 1
                
            elif(state == 1):
                #Run Waiting for Input State code
                if(initialState == 0):
                    print('Waiting for input')
                    #Display text that tells the user they need to provide input
                    initialState = 1
                    #Change initial state to 1 so the text only displays 1 time
                    #then waits for the user to press the button
                if(buttonPressed == True):
                    buttonPressed = False
                    print('Now displaying Square Wave')
                    reset_Timer()
                    state = 2       #Transition to Square Wave
                
            elif(state == 2):
                #Run Square Wave code
                if(buttonPressed == True):
                    buttonPressed = False
                    print('Now displaying Sin Wave')
                    reset_Timer()
                    state = 3       #Transition to Sin Wave
                else:
                    update_Timer()
                    t2ch1.pulse_width_percent(update_SQW(duration)) 

            elif(state == 3):
                #Run Sin Wave code
                if(buttonPressed == True):
                    buttonPressed = False
                    print('Now displaying Sawtooth Wave')
                    reset_Timer()
                    state = 4       #Transition to Sawtooth Wave
                else:
                    update_Timer()
                    t2ch1.pulse_width_percent(update_SINW(duration)) 
                
            elif(state == 4):
                #Run Sawtooth Wave code
                if(buttonPressed == True):
                    buttonPressed = False
                    print('Now displaying Square Wave')
                    reset_Timer()
                    state = 2       #Transition back to Square Wave
                else:
                    update_Timer()
                    t2ch1.pulse_width_percent(update_STW(duration)) 
                    
        except KeyboardInterrupt:
            break                     
            #Break out of loop on ctrl-c
    # Exiting the program and cleaning up    
    print('Program Terminating')   
    print('Press Ctrl+D to restart the Nucleo and the program')
        
    