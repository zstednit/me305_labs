'''
@page "Lab 1: Getting Started with Hardware"

@author Zachary Stednitz

@date 12/8/2021

@section sec_overview Overview

The purpose of this lab was to familiarize ourselves with the Nucleo and 
the STM32 Microcontroller. The task was to create 3 different light blinking
patterns that you could cycle through by pressing the button on the nucleo board.
This was achieved using a finite state machine to transition between states and 
display a different pattern each time. The FSM can be seen below:
    
@image html "Lab_1_FSM.jpg" width=50%

To see the code implementation, refer to this repository: 
    https://bitbucket.org/zstednit/me305_labs/src/master/

To see documentation for this file, visit Lab1Main.py \ref
'''