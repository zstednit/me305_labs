# -*- coding: utf-8 -*-
"""
@file:      imu_driver.py
@brief      Reads euler angles from BNO055 IMU
@author:    Zachary Stednitz
@date:      11/8/2021

"""

from pyb import I2C
import ustruct
import time
import os

# Hex number for the CALIB_STAT (pg 67 BNO Datasheet)
CALIB_STAT = 0x35
# Hex address for IMU device
# imu_address = 0x28
# Operation mode hex address and available operating modes
OPR_MODE = 0x3D
CONFIGMODE = 0b0000
# NDOF offers all 3 sensor outputs and fuses the data using absolute orientation
# Table 3-5 and Table 3-3
NDOF = 0b1100

# Calibration coefficients variables
calib_Length = 22
calib_start = 0x55  # all the way to 6A (listed register address)

# Euler Data registries
euler_start = 0x1A  # to 0x1F
euler_Length = 6

# Gyroscope Data registries
gyr_start = 0x14    # to 0x19
gyr_Length = 6

class BNO055:
    
    def __init__(self, i2c):
        ''' @brief      Creates the BNO055 object.
            @details    Initializes the imu object and sets it up to communicate
                        using the I2C protocol
            @param      i2c The I2C object used for communication
        '''
        self.i2c = i2c
    
    def set_mode(self, mode):
        ''' @brief          Changes the mode of the IMU user-specified mode
            @details        Writes the operating mode to the imu register address 
                            based on user input using I2C. 
            @param          mode    The operating mode for the IMU
        '''
        self.i2c.mem_write(mode, 0x28, OPR_MODE)
    
    def get_CalibrationStatus(self):
        ''' @brief          Get the calibration status of the IMU components
            @details        Reads the calibration status from the IMU register
                            using I2C
        '''
        return self.i2c.mem_read(1, 0x28, CALIB_STAT)
    
    def get_calibrationCoef(self):
        ''' @brief      Gets the calibration coefficients
            @details    Reads the coefficients from the IMU registers and returns them
                        These coefficients tell us whether the IMU has been calibrated. 
        '''
        # Calibration coefficients are the offsets listed in the BNO055'
        # datasheet in Table 4-2, pg 51.        
        return self.i2c.mem_read(calib_Length, 0x28, calib_start)
    
    def write_calibrationCoef(self, calibCoef):
        ''' @brief          Writes the coefficients from the IMU
            @param          calibCoef Specifies how many bytes to write to.
        '''
        self.i2c.mem_write(calibCoef, 0x28, calib_start)
    
    def read_Euler(self):
        ''' @brief          Returns the heading, pitch, and roll.
            @details        Reads the data from the IMU over I2C and returns 
                            the 3 important data points as a formatted tuple
        '''
        angles = self.i2c.mem_read(euler_Length, 0x28, euler_start)
        (heading, pitch, roll) = ustruct.unpack('hhh', angles)
        # Divide by 16 because the units must be converted, as per Table 3-29
        return heading/16, pitch/16, roll/16
    
    def read_omega(self):
        ''' @brief      Reads the gyroscope datat and returns angular velocity
            @details    Reads the data from the IMU over I2C and returns the 3 
                        velocity values as a formatted tuple.
        '''
        omegas = self.i2c.mem_read(gyr_Length, 0x28, gyr_start)
        (omega_x, omega_y, omega_z) = ustruct.unpack('hhh', omegas)
        # Divide by 16 because the units must be converted, as per Table 3-22
        return omega_x/16, omega_y/16, omega_z/16
     
if __name__== '__main__':
    i2c = I2C(1, I2C.MASTER)
    imu = BNO055(i2c)
    if 'CalibrationCoefficients' in os.listdir() :
        print('Import Calibration Coefficients...')
        imu.set_mode(CONFIGMODE)
        with open('CalibrationCoefficients', 'r') as f:
            imu.set_calibrationCoef(f.read())
        imu.set_mode(NDOF)
    else:
        # initial Calibration
        imu.set_mode(NDOF)
        status = imu.get_CalibrationStatus()[0]
        print("calibrating...")
        magCali = False
        accCali = False
        gyrCali = False
        while status != 0b11111111:
            if ((status & 0b11) == 0b11) & (magCali == False):
                print("magnetometer calibrated")
                magCali = True
            if ((status & 0b1100) == 0b1100) & (accCali == False):
                print("accelerometer calibrated")
                accCali = True
            if ((status & 0b110000) == 0b110000) & (gyrCali == False):
                print("gyroscope calibrated")
                gyrCali = True
            status = imu.get_CalibrationStatus()[0]
        print("Calibrated")
        
        # print calibration data to file
        calibration = imu.get_calibrationCoef()
        print('Calibration Coefficients')
        print(calibration)
        caliFile = open('CalibrationCoefficients', 'w')
        caliFile.write(calibration)
        caliFile.close()
    
    while True:
        angles = imu.read_Euler()
        print('(Heading, Pitch, Roll) deg')
        print(angles)
        time.sleep(1)