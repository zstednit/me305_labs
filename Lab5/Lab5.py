'''
@page "Lab 5: I2C and Inertial Measurement Units"

@author Zachary Stednitz
@author Jason Davis
@author Conor Fraser
@author Solie Grantham

@date 12/8/2021

@section sec_overview Overview

The purpose of this lab was to familiarize ourselves with the BNO055 IMU. 
The goal was to read from the right registers of the IMU and display euler angles. 
This was achieved after calibrating the IMU and finding the right register
addresses from the BNO055 datasheet. 

To see the code implementation, refer to this repository: 
    https://bitbucket.org/zstednit/me305_labs/src/master/Lab5/
    
To see documentation for this file, visit imu_driver.py \ref

'''