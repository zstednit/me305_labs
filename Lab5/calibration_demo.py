# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 16:23:46 2021

@author: zsted
"""

import struct

#######  Calibration Bytes Demo ####### 

cal_bytes = bytearray([0x5C])

print("Binary:", '{:#010b}'.format(cal_bytes[0]))

cal_status = ( cal_bytes[0] & 0b11,
              (cal_bytes[0] & 0b11 << 2) >> 2,
              (cal_bytes[0] & 0b11 << 4) >> 4,
              (cal_bytes[0] & 0b11 << 6) >> 6)

print("Values:", cal_status)
print('\n')



####### Data Unpacking Demo ####### 

eul_bytes = bytearray([0x89,    # EUL_Heading_LSB
                       0x00,    # EUL_Heading_MSB
                       0x14,    # EUL_Roll_LSB
                       0x0A,    # EUL_Roll_MSB
                       0xC4,    # EUL_Pitch_LSB
                       0xFA])   # EUL_Pitch_MSB

# Method One (manual concatenation and sign extension)
print('Bytes to Values (Method One)')
print('Raw bytes:', eul_bytes)

# First combine MSB and LSB for each value to get unsigned 16-bit integers
heading = eul_bytes[0] | eul_bytes[1] << 8  # EUL_Heading
roll    = eul_bytes[2] | eul_bytes[3] << 8  # EUL_Roll
pitch   = eul_bytes[4] | eul_bytes[5] << 8  # EUL_Pitch
print("Unsigned:", (heading, roll, pitch))

# Second, sign extend to get signed integers
if heading > 32767:
    heading -= 65536
if roll > 32767:
    roll -= 65536
if pitch > 32767:
    pitch -= 65536
print("Signed:", (heading, roll, pitch))

# Third, scale to get proper units
heading /= 16
roll /= 16
pitch /= 16
print("Scaled:", (heading, roll, pitch))
print('\n')




# Method two (using built in struct.unpack)
print('Bytes to Values (Method Two)')
print('Raw bytes:', eul_bytes)

# First, unpack bytes into three signed integers
eul_signed_ints = struct.unpack('<hhh', eul_bytes)
print('Unpacked: ', eul_signed_ints)

# Second, scale ints to get proper units
eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
print('Scaled: ', eul_vals)


# bytes_in = i2c.mem_read(4, 0x28, 0x00)
# bytes_in[0]
# hex(bytes_in[1])
# buf = bytearray(4)
# bytes_in = i2c.mem_read(buf, 0x28, 0x00)
    
# cal_bytes = self.i2c.mem_read(1, imu_address, CALIB_STAT)

#         print("Binary:", '{:#010b}'.format(cal_bytes[0]))

#         cal_status = ( cal_bytes[0] & 0b11,
#                      (cal_bytes[0] & 0b11 << 2) >> 2,
#                      (cal_bytes[0] & 0b11 << 4) >> 4,
#                      (cal_bytes[0] & 0b11 << 6) >> 6)

#         print("Values:", cal_status)
#         print('\n')